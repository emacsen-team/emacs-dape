Source: emacs-dape
Section: editors
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders:
 Xiyue Deng <manphiz@gmail.com>,
Build-Depends:
 debhelper (>= 13.24),
 debhelper-compat (= 13),
 dh-elpa,
Standards-Version: 4.7.0
Homepage: https://github.com/svaante/dape
Vcs-Browser: https://salsa.debian.org/emacsen-team/emacs-dape
Vcs-Git: https://salsa.debian.org/emacsen-team/emacs-dape.git
Rules-Requires-Root: no

Package: elpa-dape
Architecture: all
Depends:
 elpa-jsonrpc (>= 1.0.25),
 ${elpa:Depends},
 ${misc:Depends},
Recommends:
 emacs,
Enhances:
 delve,
 emacs,
 gdb,
 python3-debugpy,
Description: Debug Adapter Protocol for Emacs
 Dape is a debug adapter client for Emacs.  The debug adapter
 protocol, much like its more well-known counterpart, the language
 server protocol, aims to establish a common API for programming
 tools.  However, instead of functionalities such as code
 completions, it provides a standardized interface for debuggers.
 .
 To begin a debugging session, invoke the `dape' command.  In the
 minibuffer prompt, enter a debug adapter configuration name from
 `dape-configs'.
 .
 For complete functionality, make sure to enable `eldoc-mode' in your
 source buffers and `repeat-mode' for more pleasant key mappings.
 .
 Package looks is heavily inspired by gdb-mi.el
