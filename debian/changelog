emacs-dape (0.22.0-1) unstable; urgency=medium

  * New upstream release
  * Depends on debhelper >= 13.24 for automatic CHANGELOG.org handling
    - Drop override_dh_installchangelogs in d/control; no longer needed
  * Add explicit dependency on elpa-jsonrpc >= 1.0.25 (Closes: #1094421)
    - dh-elpa doesn't handle version of Emacs built-in packages well yet.
  * Run 'wrap-and-sort -ast'

 -- Xiyue Deng <manphiz@gmail.com>  Thu, 13 Feb 2025 00:31:13 -0800

emacs-dape (0.21.0-1) unstable; urgency=medium

  * New upstream release
  * Update copyright years in d/copyright

 -- Xiyue Deng <manphiz@gmail.com>  Sat, 11 Jan 2025 20:51:37 -0800

emacs-dape (0.19.0-1) unstable; urgency=medium

  * New upstream release
  * Override dh_installchangelogs to handle CHANGELOG.org as changelog
    - Drop CHANGELOG.org from d/docs as it is now handled by
      dh_installchangelogs.
    - Remove obsolete d/elpa-dape.lintian-overrides.
  * Disable xz compression in d/gbp.conf to be consistent with origtargz

 -- Xiyue Deng <manphiz@gmail.com>  Tue, 24 Dec 2024 02:05:51 -0800

emacs-dape (0.18.0-1) unstable; urgency=medium

  * New upstream release

 -- Xiyue Deng <manphiz@gmail.com>  Fri, 20 Dec 2024 01:46:13 -0800

emacs-dape (0.17.0-1) unstable; urgency=medium

  * New upstream release
  * Update comments in d/elpa-test to be more accurate
  * Drop Testsuite in d/control as tests are disable

 -- Xiyue Deng <manphiz@gmail.com>  Sun, 08 Dec 2024 03:24:48 -0800

emacs-dape (0.13.0-1) unstable; urgency=medium

  * Initial Debianisation (Closes: #1074569)

 -- Xiyue Deng <manphiz@gmail.com>  Tue, 02 Jul 2024 01:16:41 -0700
